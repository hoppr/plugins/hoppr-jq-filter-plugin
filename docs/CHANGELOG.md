## [0.2.5](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/compare/v0.2.4...v0.2.5) (2024-03-17)


### Bug Fixes

* **deps:** update dependency jq to v1.7.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0540154](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/05401548de3881b4757b11aa3329c475c866f166))

## [0.2.4](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/compare/v0.2.3...v0.2.4) (2023-09-18)


### Bug Fixes

* **deps:** update dependency jq to v1.6.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([69074fa](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/69074fab50c360d1c003d6b3230a90bbc8c7e7c9))

## [0.2.3](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/compare/v0.2.2...v0.2.3) (2023-08-30)


### Bug Fixes

* **deps:** update dependency jq to v1.5.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([fc61880](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/fc618809b3276e9b41417f6d3221811ae77edc7b))

## [0.2.2](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/compare/v0.2.1...v0.2.2) (2023-08-23)


### Bug Fixes

* **deps:** update dependency hoppr to v1.9.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4b3a642](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/4b3a642b344ed9668aee4e827a7109d15f65f5ae))

## [0.2.1](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/compare/v0.2.0...v0.2.1) (2023-08-01)


### Bug Fixes

* **deps:** update dependency hoppr to v1.9.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([80884b0](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/80884b044378eda41a73ff31acfdbb8a340d8e82))

## [0.2.0](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/compare/v0.1.0...v0.2.0) (2023-07-27)


### Features

* implemented deleting components found in 'collect_root_dir' ([3508495](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/350849559138aa7ddc90d88f8d11e791128bd0be))
* initial release ([874a696](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/874a696ae57f8dc83b79a5149ce784eb54b25f47))


### Bug Fixes

* Add renovate and releaserc ([0dd8176](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/0dd8176a8504da2517bd3e3ce60401d4b59093a8))
* adding additional suggestions after [#1](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/issues/1) was merged ([cb46190](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/cb461904d12cded98f53c98fe31ca0b7fa4571e7))
* addressing issues with project metadata and pipeline ([184524c](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/184524c8f2ffac0f9c01b265373a81749d6eaf77))
* code suggestions from MR. ([a7661de](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/a7661de2088c1c5ba513b00fe4a729c249158eb9))
* **deps:** update dependency hoppr to v1.9.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d345808](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/d345808dc6ea784617a29d81a22c2edc526507ad))
* **deps:** update dependency jq to v1.4.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ea7c9d7](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/ea7c9d7fda280807fa31988449ce00fd9c74f516))
* incorporating feedback from MR ([7bf1c36](https://gitlab.com/hoppr/plugins/hoppr-jq-filter-plugin/commit/7bf1c3660fe6aaff73ab51a5218bce9f50b88b98))
