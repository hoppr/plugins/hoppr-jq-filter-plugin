"""
Test module for TarBundlePlugin class
"""
from pathlib import Path

from hoppr.models import HopprContext
from hoppr_cyclonedx_models.cyclonedx_1_4 import Scope

from hoppr_jq_filter import __version__
from hoppr_jq_filter.plugin import JQFilterPlugin


def test_get_version(plugin_fixture: JQFilterPlugin):
    """
    Test what version is returned by the plugin
    """
    assert plugin_fixture.get_version() == __version__


def test_pre_stage_process_success(context_fixture: HopprContext):
    """
    Test plugin when no config is provided
    """
    plugin = JQFilterPlugin(context_fixture)
    result = plugin.pre_stage_process()
    assert result.is_success()
    assert len(result.return_obj.components) == 3

    result = plugin.post_stage_process()
    assert result.is_success()
    assert len(result.return_obj.components) == 3


def test_pre_stage_process_success_include_purl(context_fixture: HopprContext):
    """
    Test including as required where the component has "README.md" in the purl.
    """
    config = {"purl_regex_includes": ["README.md"]}

    plugin = JQFilterPlugin(context_fixture, config)
    result = plugin.pre_stage_process()

    assert result.is_success()
    assert len(result.return_obj.components) == 3

    assert result.return_obj.components[0].purl == "pkg:generic/README.md"
    assert str(result.return_obj.components[0].scope) in {'required', 'Scope.required'}

    assert result.return_obj.components[1].purl == "pkg:generic/docs/CHANGELOG.md"
    assert str(result.return_obj.components[1].scope) in {'optional', 'Scope.optional'}

    assert result.return_obj.components[2].purl == "pkg:generic/media/2Faz1t19ZifKOLqZG/200.gif@0.1.0"
    assert str(result.return_obj.components[2].scope) in {'optional', 'Scope.optional'}


def test_pre_stage_process_success_exclude_purl(context_fixture: HopprContext):
    """
    Test excluding as excluded where the component has "README.md" in the purl.
    """
    config = {"purl_regex_excludes": ["README.md"]}

    plugin = JQFilterPlugin(context_fixture, config)
    result = plugin.pre_stage_process()

    assert result.is_success()
    assert len(result.return_obj.components) == 3

    assert result.return_obj.components[0].purl == "pkg:generic/README.md"
    assert str(result.return_obj.components[0].scope) in {'excluded', 'Scope.excluded'}

    assert result.return_obj.components[1].purl == "pkg:generic/docs/CHANGELOG.md"
    assert str(result.return_obj.components[1].scope) in {'optional', 'Scope.optional'}

    assert result.return_obj.components[2].purl == "pkg:generic/media/2Faz1t19ZifKOLqZG/200.gif@0.1.0"
    assert str(result.return_obj.components[2].scope) in {'optional', 'Scope.optional'}


def test_pre_stage_process_success_include_jq(context_fixture: HopprContext):
    """
    Test including as required where the component is of type file.
    """
    config = {"jq_expression_includes": ['.components[] | select(.purl != null) | select(.type|test("file"))']}

    plugin = JQFilterPlugin(context_fixture, config)
    result = plugin.pre_stage_process()

    assert result.is_success()
    assert len(result.return_obj.components) == 3

    assert result.return_obj.components[0].purl == "pkg:generic/README.md"
    assert str(result.return_obj.components[0].scope) in {'required', 'Scope.required'}

    assert result.return_obj.components[1].purl == "pkg:generic/docs/CHANGELOG.md"
    assert str(result.return_obj.components[1].scope) in {'required', 'Scope.required'}

    assert result.return_obj.components[2].purl == "pkg:generic/media/2Faz1t19ZifKOLqZG/200.gif@0.1.0"
    assert str(result.return_obj.components[2].scope) in {'optional', 'Scope.optional'}


def test_pre_stage_process_success_exclude_jq(context_fixture: HopprContext):
    """
    Test excluding as excluded where the component is of type file.
    """
    config = {"jq_expression_excludes": ['.components[] | select(.purl != null) | select(.type|test("file"))']}

    plugin = JQFilterPlugin(context_fixture, config)
    result = plugin.pre_stage_process()

    assert result.is_success()
    assert len(result.return_obj.components) == 3
    assert result.return_obj.components[0].purl == "pkg:generic/README.md"
    assert str(result.return_obj.components[0].scope) in {'excluded', 'Scope.excluded'}

    assert result.return_obj.components[1].purl == "pkg:generic/docs/CHANGELOG.md"
    assert str(result.return_obj.components[1].scope) in {'excluded', 'Scope.excluded'}

    assert result.return_obj.components[2].purl == "pkg:generic/media/2Faz1t19ZifKOLqZG/200.gif@0.1.0"
    assert str(result.return_obj.components[2].scope) in {'optional', 'Scope.optional'}


def test_post_stage_process(context_fixture: HopprContext):
    """
    Test excluding as excluded where the component is of type file.
    """
    config = {"delete_excluded": False}

    context_fixture.delivered_sbom.components[0].scope = Scope.excluded

    component_path = Path(
        context_fixture.collect_root_dir  # type: ignore
        / context_fixture.delivered_sbom.components[0].properties[0].value
    )
    component_path.mkdir(exist_ok=True)
    component_file_path = component_path / "test-file"
    component_file_path.touch()

    plugin = JQFilterPlugin(context_fixture, config)
    result = plugin.post_stage_process()

    assert result.is_success()
    assert component_file_path.exists()

    config = {"delete_excluded": True}
    plugin = JQFilterPlugin(context_fixture, config)
    result = plugin.post_stage_process()

    assert result.is_success()
    assert not component_file_path.exists()
