"""
Shareable fixtures for unit tests
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import multiprocessing
import time
import typing

from pathlib import Path
from subprocess import CompletedProcess
from typing import Callable

import pytest
import requests

from hoppr.base_plugins.hoppr import HopprPlugin
from hoppr.exceptions import HopprLoadDataError
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService
from hoppr.models.manifest import Manifest
from hoppr.models.sbom import Component, Sbom
from packaging.version import InvalidVersion
from packaging.version import parse as parse_version
from pytest import Config, FixtureRequest, MonkeyPatch, TempPathFactory
from requests import HTTPError


@pytest.fixture(autouse=True)
def print_test_name(request: FixtureRequest):
    """
    Print name of test
    """
    print(f"-------- Starting {request.node.name} --------")


@pytest.fixture(autouse=True)
def sleepless(monkeypatch: MonkeyPatch):
    """
    Spend less time in sleep
    """
    monkeypatch.setattr(target=time, name="sleep", value=lambda secs: None)


@pytest.fixture
def collect_root_dir_fixture(request: FixtureRequest, tmp_path_factory: TempPathFactory) -> Path:
    """
    Parametrizable temp dir fixture
    """
    basename = getattr(request, "param", "hoppr_unit_test")
    return tmp_path_factory.mktemp(basename)


@pytest.fixture
def completed_process_fixture(request: FixtureRequest) -> CompletedProcess:
    """
    CompletedProcess fixture for subprocess commands

    Intended for use with `pytest` test modules. For `unittest.TestCase` based
    classes, use the `test.mock_objects.MockSubprocessRun` mock object.
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["args"] = param_dict.get("args", ["mock", "command"])
    param_dict["returncode"] = param_dict.get("returncode", 0)
    param_dict["stdout"] = param_dict.get("stdout", b"Mock stdout")
    param_dict["stderr"] = param_dict.get("stderr", b"Mock stderr")

    return CompletedProcess(**param_dict)


@pytest.fixture
def config_fixture(request: FixtureRequest) -> dict[str, str]:
    """
    Test plugin config fixture
    """
    return dict(getattr(request, "param", {}))


@pytest.fixture
def context_fixture(
    request: FixtureRequest, manifest_fixture: Manifest, collect_root_dir_fixture: Path
) -> HopprContext:
    """
    Test Context fixture
    """
    param_dict = dict(getattr(request, "param", {}))
    strict_repos = param_dict.get("strict_repos", True)

    return HopprContext(
        collect_root_dir=collect_root_dir_fixture,
        consolidated_sbom=manifest_fixture.consolidated_sbom.copy(deep=True),
        credential_required_services=None,
        delivered_sbom=manifest_fixture.consolidated_sbom.copy(deep=True),
        logfile_lock=multiprocessing.Manager().RLock(),
        max_processes=3,
        repositories=manifest_fixture.repositories,
        sboms=list(Sbom.loaded_sboms.values()),
        stages=[],
        strict_repos=strict_repos,
    )


@pytest.fixture
def cred_object_fixture(request: FixtureRequest, monkeypatch: MonkeyPatch) -> CredentialRequiredService:
    """
    Test CredObject fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["url"] = param_dict.get("url", "https://test.hoppr.com")
    param_dict["user"] = param_dict.get("user", "mock_user")
    param_dict["user_env"] = param_dict.get("pass_env", "MONKEYPATCH_USER_ENV")
    param_dict["pass_env"] = param_dict.get("pass_env", "MONKEYPATCH_PASS_ENV")
    param_dict["password"] = param_dict.get("password", "mock_password")

    monkeypatch.setenv(name=param_dict["user_env"], value=param_dict["user"])
    monkeypatch.setenv(name=param_dict["pass_env"], value=param_dict["password"])

    return CredentialRequiredService.parse_obj(param_dict)


@pytest.fixture
def find_credentials_fixture(
    cred_object_fixture: CredentialRequiredService,
) -> Callable[[str], CredentialRequiredService]:
    """
    Fixture to use when monkeypatching `Credentials.find` method
    """

    def _find_credentials(url: str) -> CredentialRequiredService:
        return cred_object_fixture

    return _find_credentials


@pytest.fixture
def load_url_fixture(request: FixtureRequest) -> Callable[[str], object]:
    """
    Fixture to patch hoppr.net.load_url
    """

    def _load_url(url: str):
        if request.param == "HTTPError":
            raise HTTPError

        if request.param == "HopprLoadDataError":
            raise HopprLoadDataError

        return request.param

    return _load_url


@pytest.fixture
def manifest_fixture(request: FixtureRequest, resources_dir: Path) -> Manifest:
    """
    Test Manifest fixture

    Parametrization options:
        manifest_path (Path): Path object of manifest file to load
    """
    param_dict = dict(getattr(request, "param", {}))
    manifest_path = param_dict.get("manifest_path", resources_dir / "manifest.yml")

    Manifest.loaded_manifests.clear()
    Sbom.loaded_sboms.clear()
    Sbom.unique_id_map.clear()
    Component.unique_id_map.clear()

    return Manifest.load(manifest_path)


@pytest.fixture
def plugin_fixture(
    request: FixtureRequest, config_fixture: dict[str, str] | None, context_fixture: HopprContext
) -> HopprPlugin:
    """
    Test collector plugin fixture

    The class type of Hoppr plugin instance returned is determined from the annotated
    type hint of the test function's `plugin` argument, if it is a subclass of
    `HopprPlugin`. Otherwise, a TypeError is raised. For example:

    ```python
    def test_collect(plugin: CollectRawPlugin):
        ...
    ```

    will provide a `plugin` argument with a type of `CollectRawPlugin`.
    """
    try:
        # Get type of plugin to return from test function's type hints
        plugin_cls = typing.get_type_hints(request.function)["plugin_fixture"]
        assert issubclass(plugin_cls, HopprPlugin), f"{plugin_cls}"

        plugin_obj = plugin_cls(context_fixture, config_fixture)
        version = plugin_obj.get_version()

        parse_version(version)
    except KeyError as ex:
        raise TypeError("Type hint must be provided in order to use plugin_fixture.") from ex
    except AssertionError as ex:
        raise TypeError(f"Type hint provided to plugin_fixture not a subclass of HopprPlugin: '{ex}'") from ex
    except InvalidVersion as ex:
        pytest.fail(f"{ex}")

    return plugin_obj


@pytest.fixture
def proxy_fixture(request: FixtureRequest) -> dict[str, str]:
    """
    Fixture to return a dict in the format used by `urllib.request.getproxies`
    """
    proxy_dict = dict(getattr(request, "param", {}))

    proxy_dict["all"] = proxy_dict.get("all", "http://proxy.hoppr-test.com")
    proxy_dict["http"] = proxy_dict.get("http", "http://proxy.hoppr-test.com")
    proxy_dict["https"] = proxy_dict.get("https", "http://proxy.hoppr-test.com")
    proxy_dict["no"] = proxy_dict.get("no", "127.0.0.1,localhost,local,golang-test.com")

    return proxy_dict


@pytest.fixture
def patch_proxies_fixture(proxy_fixture: dict[str, str], monkeypatch: MonkeyPatch):
    """
    Fixture for patching proxy-related functions used by _repo_proxy
    """
    for protocol, proxy in proxy_fixture.items():
        monkeypatch.setenv(name=f"{protocol}_proxy", value=proxy)
        monkeypatch.setenv(name=f"{protocol.upper()}_PROXY", value=proxy)


@pytest.fixture
def readmodule_fixture() -> Callable[[str], dict[str, None]]:
    """
    Fixture to use for patching `pyclbr.readmodule` method
    """

    def _readmodule(module: str) -> dict[str, None]:  # pylint: disable=unused-argument
        module_classes = {}

        match module:
            case "hoppr.core_plugins.bundle_tar":
                module_classes = {"TarBundlePlugin": None}
            case "hoppr.core_plugins.collect_apt_plugin":
                module_classes = {"CollectAptPlugin": None}
            case "hoppr.core_plugins.collect_cargo_plugin":
                module_classes = {"CollectCargoPlugin": None}
            case "hoppr.core_plugins.collect_dnf_plugin":
                module_classes = {"CollectDnfPlugin": None}
            case "hoppr.core_plugins.collect_docker_plugin":
                module_classes = {"CollectDockerPlugin": None}
            case "hoppr.core_plugins.collect_git_plugin":
                module_classes = {"CollectGitPlugin": None}
            case "hoppr.core_plugins.collect_helm_plugin":
                module_classes = {"CollectHelmPlugin": None}
            case "hoppr.core_plugins.collect_maven_plugin":
                module_classes = {"CollectMavenPlugin": None}
            case "hoppr.core_plugins.collect_nexus_search":
                module_classes = {"CollectNexusSearch": None}
            case "hoppr.core_plugins.collect_nuget_plugin":
                module_classes = {"CollectNugetPlugin": None}
            case "hoppr.core_plugins.collect_pypi_plugin":
                module_classes = {"CollectPypiPlugin": None}
            case "hoppr.core_plugins.collect_raw_plugin":
                module_classes = {"CollectRawPlugin": None}
            case "hoppr.core_plugins.collect_yum_plugin":
                module_classes = {"CollectYumPlugin": None}
            case "hoppr.core_plugins.composite_collector":
                module_classes = {"CompositeCollector": None}
            case "hoppr.core_plugins.delta_sbom":
                module_classes = {"DeltaSbom": None}
            case "hoppr.core_plugins.oras_bundle":
                module_classes = {"OrasBundlePlugin": None}
            case "hoppr.core_plugins.report_generator.report_generator":
                module_classes = {"ReportGenerator": None}
            case "test.unit.test_utils":
                module_classes = {"UnitTestUtilsPlugin": None}
            case "test.unit.test_processor":
                module_classes = {"UnitTestPlugin": None}
            case _:
                module_classes = {}

        return module_classes

    return _readmodule


@pytest.fixture
def response_fixture(request: FixtureRequest) -> Callable[[str], requests.Response]:
    """
    Test fixture to return a parametrized requests.Response
    """
    test_input_data = (
        b'{"alpha": [1, 2, 3], "beta": ["dog", "cat"], "gamma": {"x": 42, "y": "why not", "z": ["mixed", 7, "array"]}}'
    )

    def _response(*args, **kwargs) -> requests.Response:
        """
        Implemented function to be referenced for response_fixture to return a Callable
        """
        param_dict = dict(getattr(request, "param", {}))

        response = requests.Response()
        response.status_code = param_dict.get("status_code", 200)
        response._content = param_dict.get("content", test_input_data)  # pylint: disable=protected-access
        response.raw = response.content
        response.reason = param_dict.get("reason", "")
        response.url = param_dict.get("url", "http://mock.url")

        return response

    return _response


@pytest.fixture(name="resources_dir", scope="session")
def resources_dir_fixture(pytestconfig: Config) -> Path:
    """
    Fixture to return Path object representing test/resources directory
    """
    return pytestconfig.rootpath / "test" / "resources"


@pytest.fixture
def run_command_fixture(
    completed_process_fixture: CompletedProcess,
) -> Callable[[list[str], list[str] | None, str | None], CompletedProcess]:
    """
    Override completed_process_fixture to return Callable
    """

    def _run_command(
        command: list[str], password_list: list[str] | None = None, cwd: str | None = None, timeout: float | None = 60
    ) -> CompletedProcess:
        return completed_process_fixture

    return _run_command
